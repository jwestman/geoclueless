import sys

class Location:
    def __init__(self, lat, lon, accuracy=0, altitude=sys.float_info.min,
                 speed=-1, heading=-1, delay=0):
        self.lat = lat
        self.lon = lon
        self.accuracy = accuracy
        self.altitude = altitude
        self.speed = speed
        self.heading = heading
        self.delay = delay

    @staticmethod
    def from_args(args):
        kwargs = {}
        if args.accuracy is not None: kwargs["accuracy"] = args.accuracy
        if args.altitude is not None: kwargs["altitude"] = args.altitude
        if args.heading is not None:  kwargs["heading"] = args.heading

        return Location(args.latitude, args.longitude, **kwargs)

    def __str__(self):
        return "{}, {}".format(self.lat, self.lon)
