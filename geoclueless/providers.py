import csv, datetime, io

from geoclueless import untangle
from geoclueless.location import Location
from geoclueless.main import get_args

def get_provider():
    args = get_args()
    if args.datasource == "coords":
        return StaticProvider(Location.from_args(args))
    elif args.datasource == "csv":
        args.file.seek(0)
        return CsvProvider(args.file)
    elif args.datasource == "gpx":
        args.file.seek(0)
        return GpxProvider(io.StringIO(args.file.read()))


def _strptime(string):
    return datetime.datetime.strptime(string, "%Y-%m-%dT%H:%M:%SZ")

class StaticProvider:
    def __init__(self, location):
        self.location = location

    def __iter__(self):
        return iter([self.location])

class CsvProvider:
    def __init__(self, file):
        self.reader = csv.reader(file)
        self.delay = 1.0 / get_args().rate

    def __iter__(self):
        return self

    def __next__(self):
        lat, lon, *extra = next(self.reader)
        return Location(float(lat), float(lon), delay=self.delay)

class GpxProvider:
    def __init__(self, file):
        self.xml = untangle.parse(file)
        self.elements = iter(self.xml.gpx.trk.trkseg.trkpt)
        self.last = None
        self.speed = get_args().speed

    def __iter__(self):
        return self

    def __next__(self):
        el = next(self.elements)

        args = {}

        lat = float(el["lat"])
        lon = float(el["lon"])

        try:
            args["altitude"] = float(el.ele.cdata)
        except: pass

        try:
            args["speed"] = float(el.extensions.speed.cdata) * self.speed
        except: pass

        if self.last is not None:
            time = _strptime(el.time.cdata)
            last_time = _strptime(self.last.time.cdata)
            args["delay"] = max(0, (time - last_time).total_seconds()) / self.speed
        self.last = el

        return Location(lat, lon, **args)
