from gi.repository import GLib, Gio, Geoclue

from geoclueless.interfaces.client import CLIENT
from geoclueless.dbus_location import DBusLocation
from geoclueless.main import BUS_NAME

class DBusClient:
    def __init__(self, manager, sender, num):
        self.nodeinfo = Gio.DBusNodeInfo.new_for_xml(CLIENT)
        self.manager = manager
        self.sender = sender
        self.num = num
        self.path = "/org/freedesktop/GeoClue2/Client/" + str(num)

        self.distance_threshold = 0
        self.time_threshold = 0
        self.active = False
        self.location = None
        self.old_location = None

        self.registration_id = 0

    def register(self, conn):
        self.conn = conn

        self.registration_id = conn.register_object(
            interface_info=self.nodeinfo.interfaces[0],
            object_path=self.path,
            method_call_closure=self.on_method_call,
            get_property_closure=self.on_get_property,
            set_property_closure=self.on_set_property
        )

    def unregister(self):
        self.conn.unregister_object(self.registration_id)
        self.location.unregister(self.conn)

    def update(self, location):
        old_location = self.location
        new_location = DBusLocation(location)
        new_location.register(self.conn)

        if self.old_location is not None: self.old_location.unregister(self.conn)
        self.location = new_location
        self.old_location = old_location

        self.conn.emit_signal(
            None, self.path, "org.freedesktop.GeoClue2.Client",
            "LocationUpdated",
            GLib.Variant("(oo)",
                (old_location.path if old_location is not None else "/",
                new_location.path)
            )
        )

    def on_method_call(self, conn, sender, obj_path, interface, method, params, invocation):
        if method == "Start":
            self.active = True
            if self.manager.current_location is not None:
                self.update(self.manager.current_location)
        elif method == "Stop":
            self.active = False

        invocation.return_value(None)

    def on_get_property(self, conn, sender, obj_path, interface, property):
        if property == "Location":
            if self.location is None:
                return GLib.Variant("o", "/")
            else:
                return GLib.Variant("o", self.location.path)
        elif property == "DistanceThreshold":
            return GLib.Variant("u", self.distance_threshold)
        elif property == "TimeThreshold":
            return GLib.Variant("u", self.distance_threshold)
        elif property == "DesktopId":
            return GLib.Variant("s", "")
        elif property == "RequestedAccuracyLevel":
            return GLib.Variant("u", Geoclue.AccuracyLevel.EXACT)
        elif property == "Active":
            return GLib.Variant("b", True)

    def on_set_property(self, *args):
        return True # yeah sure whatever
