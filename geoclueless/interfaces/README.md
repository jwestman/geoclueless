# GeoClue DBus Interfaces

These XML files have been taken from the GeoClue source code at
https://gitlab.freedesktop.org/geoclue/geoclue/tree/master/interface.
GeoClue is licensed under the GPL version 2 or later.
