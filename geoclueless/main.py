import subprocess, argparse

import gi
gi.require_version("Geoclue", "2.0")
from gi.repository import Gio, GLib

BUS_NAME = "org.freedesktop.GeoClue2"
ARGS = None
def get_args():
    return ARGS
from geoclueless.dbus_manager import DBusManager

def main():
    collect_args()

    # Kill any existing GeoClue process
    kill_existing_geoclue()

    # Start the main loop
    GLib.MainLoop.new(None, True).run()

def collect_args():
    global ARGS

    parser = argparse.ArgumentParser(description="Fake your GPS location")
    subparsers = parser.add_subparsers(help="data sources", required=True, metavar="{coords|csv|gpx}", dest="datasource")

    parser.add_argument("-q", "--quiet", action="store_true", help="Do not output coordinates as they are replayed")
    parser.add_argument("-l", "--loop", action="store_true", help="Go back to the beginning instead of stopping at the end of the track")

    coords = subparsers.add_parser("coords", description="Set location to a specific point")
    coords.add_argument("latitude", type=float)
    coords.add_argument("longitude", type=float)
    coords.add_argument("--altitude", type=float, help="Altitude in meters")
    coords.add_argument("--accuracy", type=float, help="Accuracy in meters")
    coords.add_argument("--heading", type=float, help="Heading in degrees clockwise from due north")

    csv = subparsers.add_parser("csv", description="Set location to points defined in a CSV file or stream")
    csv.add_argument("file", type=argparse.FileType(), help="CSV file to read")
    csv.add_argument("--rate", type=float, default=1, help="Number of points to read per second")

    gpx = subparsers.add_parser("gpx", description="Replay a GPX track")
    gpx.add_argument("file", type=argparse.FileType(), help="GPX file to read, or - for stdin")
    gpx.add_argument("--speed", type=float, default=1, help="Speed at which to replay track. 1 is realtime.")

    ARGS = parser.parse_args()

def start_server():
    """ Start the custom GeoClue2 server and set up the Manager object. """

    def bus_acquired(conn, name):
        pass

    def name_acquired(conn, name):
        manager = DBusManager()
        manager.register(conn)
        print("Waiting for DBus client...")

    def name_lost(conn, name):
        print("Could not aqcuire name. Make sure to run with sudo.")
        exit(1)

    Gio.bus_own_name(
        Gio.BusType.SYSTEM,
        BUS_NAME,
        Gio.BusNameOwnerFlags.DO_NOT_QUEUE,
        bus_acquired,
        name_acquired,
        name_lost
    )

def kill_existing_geoclue():
    """ Find the process that currently owns the org.freedesktop.GeoClue2 bus
    name, and kill it so we can replace it (if it exists). """

    def on_get_pid(proxy, pid, userdata):
        p = subprocess.Popen(["kill", str(pid)])
        p.wait()

        if p.returncode != 0:
            print("Could not kill existing GeoClue. Rerun with sudo.")
            exit(1)
        else:
            print("Killed existing GeoClue process")
            start_server()

    def on_error(*args):
        print("No existing GeoClue process")
        start_server()

    def on_proxy_create(proxy, task):
        proxy.GetConnectionUnixProcessID(
            "(s)", BUS_NAME,
            result_handler=on_get_pid,
            error_handler=on_error
        )

    proxy = Gio.DBusProxy.new_for_bus(
        Gio.BusType.SYSTEM,
        Gio.DBusProxyFlags.NONE,
        None,
        "org.freedesktop.DBus",
        "/org/freedesktop/DBus",
        "org.freedesktop.DBus",
        None,
        on_proxy_create)
