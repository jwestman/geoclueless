import time, math

from gi.repository import GLib, Gio

from geoclueless.interfaces.location import LOCATION

num = 0

class DBusLocation:
    def __init__(self, location):
        global num

        self.nodeinfo = Gio.DBusNodeInfo.new_for_xml(LOCATION)
        self.num = num
        num += 1
        self.path = "/org/freedesktop/GeoClue2/Location/" + str(self.num)
        self.location = location

        t = time.time()
        self.timestamp = (math.floor(t), t % 1)

        self.registration_id = 0

    def register(self, conn):
        self.registration_id = conn.register_object(
            interface_info=self.nodeinfo.interfaces[0],
            object_path=self.path,
            method_call_closure=self.on_method_call,
            get_property_closure=self.on_get_property,
            set_property_closure=self.on_set_property
        )

    def unregister(self, conn):
        conn.unregister_object(self.registration_id)

    def on_method_call(self, conn, sender, obj_path, interface, method, params, invocation):
        pass

    def on_get_property(self, conn, sender, obj_path, interface, property):
        if property == "Latitude":
            return GLib.Variant("d", self.location.lat)
        elif property == "Longitude":
            return GLib.Variant("d", self.location.lon)
        elif property == "Accuracy":
            return GLib.Variant("d", self.location.accuracy)
        elif property == "Altitude":
            return GLib.Variant("d", self.location.altitude)
        elif property == "Speed":
            return GLib.Variant("d", self.location.speed)
        elif property == "Heading":
            return GLib.Variant("d", self.location.heading)
        elif property == "Description":
            return GLib.Variant("s", "")
        elif property == "Timestamp":
            return GLib.Variant("(tt)", self.timestamp)

    def on_set_property(self, *args):
        return True
