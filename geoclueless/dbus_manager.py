from gi.repository import GLib, Gio, Geoclue

from geoclueless.interfaces.manager import MANAGER
from geoclueless.dbus_client import DBusClient
from geoclueless import providers
from geoclueless.main import get_args

class DBusManager:
    def __init__(self):
        self.nodeinfo = Gio.DBusNodeInfo.new_for_xml(MANAGER)
        self.client_num = 0
        self.clients = {}
        self.provider = iter(providers.get_provider())
        self.current_location = None

        self.quiet = get_args().quiet

        self.in_use = False

    def register(self, conn):
        conn.register_object(
            interface_info=self.nodeinfo.interfaces[0],
            object_path="/org/freedesktop/GeoClue2/Manager",
            method_call_closure=self.on_method_call,
            get_property_closure=self.on_get_property,
            set_property_closure=self.on_set_property
        )

    def on_method_call(self, conn, sender, obj_path, interface, method, params, invocation):
        if method == "AddAgent":
            invocation.return_value(None)
        elif method == "CreateClient":
            client = self.create_client(conn, sender)
            invocation.return_value(GLib.Variant("(o)", (client.path,)))
        elif method == "GetClient":
            for client in self.clients.values():
                if client.sender == sender:
                    invocation.return_value(GLib.Variant("(o)", (client.path,)))

            client = self.create_client(conn, sender)
            invocation.return_value(GLib.Variant("(o)", (client.path,)))
        elif method == "DeleteClient":
            client = self.clients.get(params[0])
            if client is not None:
                client.unregister()
                self.clients.pop(params[0])
            invocation.return_value(None)

    def on_get_property(self, conn, sender, obj_path, interface, property):
        if property == "InUse":
            return GLib.Variant("b", self.in_use)
        elif property == "AvailableAccuracyLevel":
            return GLib.Variant("u", Geoclue.AccuracyLevel.EXACT)

    def on_set_property(self, *args):
        return True

    def create_client(self, conn, sender):
        client = DBusClient(self, sender, self.client_num)
        client.register(conn)
        self.client_num += 1
        self.clients[client.path] = client

        if not self.in_use:
            self.in_use = True
            self.next_location()

        return client

    def next_location(self):
        location = None

        def on_timeout():
            if not self.quiet: print(location)

            self.current_location = location
            for client in self.clients.values():
                client.update(location)
            self.next_location()

        try:
            location = next(self.provider)
        except StopIteration:
            if get_args().loop:
                self.provider = iter(providers.get_provider())
                location = next(self.provider)

        if location is not None:
            GLib.timeout_add(int(location.delay * 1000), on_timeout)
