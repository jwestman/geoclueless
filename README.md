# GeoClueless
A location faking tool for GeoClue2

## What it does
GeoClueless allows you to fake your computer's geographical location for
debugging purposes. While it is running, applications that use GeoClue can
be fed coordinates of your choosing rather than your actual location.

One of the most useful features is GPX track replaying. This allows you to
record a track on your phone with an app like OsmAnd, then copy it to your
computer and replay it in real time. This way, you can pretend to drive the
same route over and over again without leaving your chair.

## How it works
GeoClueless implements the GeoClue D-Bus API, but with debug data instead of
real data. When you run it, it kills the existing GeoClue process if necessary,
then sets itself up on the `org.freedesktop.GeoClue2` name.

## Requirements/Installation
- Python 3
- [PyGObject](https://pypi.org/project/PyGObject/) (probably also available
    through your distro's package repositories)

To install, simply clone this repository with git. Then run the program with
`sudo ./geoclueless.py`.

## Usage

### General Options
- `-q`, `--quiet`: Do not output coordinates as they are replayed
- `-l`, `--loop`: Go back to the beginning instead of stopping at the end of
the track

### Data Sources
You can manually input coordinates, source them from a CSV file, or replay a
GPX track file.

#### Manually
`sudo ./geoclueless.py coords <lat> <lon>`

Other options:
- `--altitude`: The altitude (meters) of the point
- `--accuracy`: The accuracy (meters) to report
- `--heading`: The heading to report, in degrees clockwise from due north

#### CSV
`sudo ./geoclueless.py csv <file>`

Other options:
- `--rate`: Number of points per second

#### GPX
`sudo ./geoclueless.py gpx <file>`

Other options:
- `--speed`: The speed at which to replay the track. 1 is real time, .5 is half
speed, etc.

## Examples
There are example input files in the examples/ directory:
- london.csv: A simple, 60-line CSV file that goes in a square around London

## Notes
- For CSV and GPX options, replay will not start until at least one client has
connected on D-Bus.
- You must run GeoClueless with `sudo` so it can register itself the system bus.
- GeoClueless does not perfectly replicate the behavior of GeoClue. In
particular, it ignores the authorization and accuracy scrambling features of
GeoClue. If you need these features emulated in GeoClueless, please file an
issue or (preferably) a merge request.
- So far, I've only tested the GPX feature with tracks generated in OsmAnd. If
you have a different app and it doesn't work, please file an issue.

## License
GeoClueless is licensed under the GNU General Public License, version 3 or
later. See COPYING.

GeoClueless contains a few files adapted from the original GeoClue. See
geoclueless/interfaces/README.md.

Untangle is a simple XML parsing library for Python. Its source code has been
copied to geoclueless/untangle.py. Untangle is licensed under the MIT license
and its source code is at https://github.com/stchris/untangle.
